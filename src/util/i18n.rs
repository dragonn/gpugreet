use i18n_embed::{
    fluent::{fluent_language_loader, FluentLanguageLoader},
    DesktopLanguageRequester, LanguageLoader,
};

use rust_embed::RustEmbed;

#[derive(RustEmbed)]
#[folder = "contrib/locales"]
struct Localizations;

use lazy_static::lazy_static;

lazy_static! {
    pub static ref MESSAGES: FluentLanguageLoader = {
        let locales = Localizations;
        let loader = fluent_language_loader!();
        loader.load_languages(&locales, &[loader.fallback_language()]).unwrap();

        let _ = i18n_embed::select(&loader, &locales, &DesktopLanguageRequester::requested_languages());

        loader
    };
}

#[macro_use]
macro_rules! i18n {
  ($message_id:literal) => {{
    i18n_embed_fl::fl!($crate::util::i18n::MESSAGES, $message_id)
  }};

  ($message_id:literal, $($args:expr),*) => {{
    i18n_embed_fl::fl!($crate::util::i18n::MESSAGES, $message_id, $($args), *)
  }};
}

pub(crate) use i18n;
