use termion::raw::RawTerminal;
use tui::{
    backend::TermionBackend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, List, ListItem},
    Frame,
};

use std::{
    error::Error,
    io::{self, Write},
};

use crate::app::App;

pub fn draw(app: &mut App, f: &mut Frame<TermionBackend<RawTerminal<io::Stdout>>>) {
    let size = f.size();

    let width = 60;
    let height = 5;
    let container_padding = 0;

    let x = (size.width - width) / 2;
    let y = (size.height - height) / 2;

    let container = Rect::new(x, y, width, height);
    let frame = Rect::new(x + container_padding, y + container_padding, width - (2 * container_padding), height - (2 * container_padding));

    let block = Block::default().title("Select GPU mode").borders(Borders::ALL).border_type(BorderType::Plain);

    let items: Vec<ListItem> = app
        .modes
        .iter()
        .map(|i| {
            let mut lines = vec![];
            lines.push(Spans::from(Span::styled(format!("{}", i), Style::default())));
            ListItem::new(lines).style(Style::default())
        })
        .collect();

    // Create a List from all list items and highlight the currently selected one
    let items = List::new(items)
        .block(Block::default().borders(Borders::ALL).title("List"))
        .highlight_style(Style::default().bg(Color::LightGreen).add_modifier(Modifier::BOLD).fg(Color::Black))
        .highlight_symbol(">> ");

    // We can now render the item list
    f.render_stateful_widget(items, frame, &mut app.items.state);

    f.render_widget(block, container);
}
