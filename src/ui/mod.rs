mod prompt;

use termion::raw::RawTerminal;
use tui::{
    backend::TermionBackend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, List, ListItem, Paragraph},
    Frame, Terminal,
};

use std::{
    error::Error,
    io::{self, Write},
};

use chrono::prelude::*;

use crate::{app::App, util::i18n};

use crate::i18n::i18n;

const TITLEBAR_INDEX: usize = 1;
const STATUSBAR_INDEX: usize = 3;
const STATUSBAR_LEFT_INDEX: usize = 1;
const STATUSBAR_RIGHT_INDEX: usize = 2;

#[macro_use]
pub fn draw(app: &mut App, terminal: &mut Terminal<TermionBackend<RawTerminal<io::Stdout>>>) {
    terminal
        .draw(|mut f| {
            let container_padding = 0;

            let size = f.size();
            let chunks = Layout::default()
                .constraints(
                    [
                        Constraint::Length(0), // Top vertical padding
                        Constraint::Length(1), // Date and time
                        Constraint::Min(1),    // Main area
                        Constraint::Length(1), // Status line
                        Constraint::Length(0), // Bottom vertical padding
                    ]
                    .as_ref(),
                )
                .split(size);

            let time_text = Span::from(get_time());
            let time = Paragraph::new(time_text).alignment(Alignment::Center);

            f.render_widget(time, chunks[TITLEBAR_INDEX]);

            let status_chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Length(container_padding), Constraint::Percentage(50), Constraint::Percentage(50), Constraint::Length(container_padding)].as_ref())
                .split(chunks[STATUSBAR_INDEX]);

            let status_left_text = Spans::from(vec![status_label("Q"), status_value(i18n!("action_exit")), status_label("Enter"), status_value(i18n!("action_select"))]);
            let status_left = Paragraph::new(status_left_text);

            f.render_widget(status_left, status_chunks[STATUSBAR_LEFT_INDEX]);

            prompt::draw(app, f);
        })
        .expect("wtf");
}

fn get_time() -> String {
    Local::now().format_localized(&i18n!("date"), Locale::en_US).to_string()
}

fn status_label<'s, S>(text: S) -> Span<'s>
where
    S: Into<String>,
{
    Span::styled(text.into(), Style::default().add_modifier(Modifier::REVERSED))
}

fn status_value<'s, S>(text: S) -> Span<'s>
where
    S: Into<String>,
{
    Span::from(format!(" {} ", (&text.into())))
}
