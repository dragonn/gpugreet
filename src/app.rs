use crate::{backends::Mode, util::StatefulList};

pub struct App<'a> {
    pub modes: &'a Vec<Mode<'a>>,
    pub items: StatefulList<Mode<'a>>,
}

impl<'a> App<'a> {
    pub fn new(modes: &'a Vec<Mode>) -> App<'a> {
        App {
            modes,
            items: StatefulList::with_items(modes.to_vec()),
        }
    }
}
