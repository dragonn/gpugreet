use std::{borrow::Cow, error::Error, process::Command};

use super::{Backend, Mode};

use indexmap::IndexMap;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
struct ModeConfig {
    desc: String,
    commands: Vec<String>,
    auto: Option<toml::Value>,
}

impl ModeConfig {
    pub fn apply(&self) -> Result<Vec<String>, Box<dyn Error>> {
        let mut outputs = Vec::with_capacity(self.commands.len());
        for cmd in &self.commands {
            outputs.push(Self::run_command(cmd)?);
        }

        Ok(outputs)
    }

    fn run_command(command: &str) -> Result<String, Box<dyn Error>> {
        let command: Vec<&str> = command.split(" ").collect();
        Ok(String::from_utf8(Command::new(command[0]).args(&command[1..]).output()?.stdout)?)
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type", content = "options")]
enum DefaultConfig {
    Fixed(String),
    Saved,
}

#[derive(Debug, Deserialize)]
pub struct BuiltinConfig {
    default: DefaultConfig,
    #[serde(default)]
    debug: bool,
    #[serde(with = "indexmap::serde_seq")]
    modes: IndexMap<String, ModeConfig>,
}

impl BuiltinConfig {
    pub fn to_backend(&self) -> BuiltinBackend {
        BuiltinBackend::new(self)
    }

    pub fn list_modes(&self) -> Vec<Mode> {
        self.modes.iter().map(|(key, mode)| Mode::new(Cow::Borrowed(&key), Cow::Borrowed(&mode.desc))).collect()
    }
}

pub struct BuiltinBackend<'a> {
    config: &'a BuiltinConfig,
}

impl<'a> BuiltinBackend<'a> {
    pub fn new(config: &'a BuiltinConfig) -> Self {
        Self { config }
    }
}

impl Backend for BuiltinBackend<'_> {
    fn list_modes(&self) -> Vec<Mode> {
        return self.config.list_modes();
    }

    fn default(&self) -> Option<usize> {
        match &self.config.default {
            DefaultConfig::Fixed(fixed) => self.config.modes.get_index_of(fixed),
            _ => None,
        }
    }

    fn apply_mode(&self, select: Option<usize>) -> Result<(), Box<dyn Error>> {
        if let Some((_, mode)) = self.config.modes.get_index(select.ok_or("mode not select")?) {
            let output = mode.apply()?;
            if self.config.debug {
                println!("debug: {:?}", output);
                std::thread::sleep(std::time::Duration::from_secs(2));
            }
        }
        Ok(())
    }
}
