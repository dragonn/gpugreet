use std::borrow::Cow;

pub mod builtin;
use std::error::Error;
use std::fmt;

#[derive(Debug, Clone)]
pub struct Mode<'a> {
    name: Cow<'a, str>,
    desc: Cow<'a, str>,
}

impl<'a> Mode<'a> {
    pub fn new(name: Cow<'a, str>, desc: Cow<'a, str>) -> Self {
        Self { name, desc }
    }
}

impl fmt::Display for Mode<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} ({})", self.desc, self.name)
    }
}

pub trait Backend {
    fn list_modes(&self) -> Vec<Mode>;
    fn default(&self) -> Option<usize>;
    fn apply_mode(&self, select: Option<usize>) -> Result<(), Box<dyn Error>>;
}
