mod util;

#[macro_use]
use crate::util::i18n;

mod app;
mod backends;
mod config;
mod ui;

use crate::util::event::{Event, Events};

use crate::app::App;

use std::process::Command;
use std::time;
use std::{error::Error, io, time::Instant};
use termion::{event::Key, input::MouseTerminal, raw::IntoRawMode, screen::AlternateScreen};
use tui::{
    backend::TermionBackend,
    layout::{Constraint, Corner, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, List, ListItem},
    Terminal,
};

use gumdrop::Options;

#[derive(Debug, Options)]
struct CmdOptions {
    #[options(help = "command to execute after exit")]
    exec: Option<String>,

    #[options(help = "print help message")]
    help: bool,
}

fn main() -> Result<(), Box<dyn Error>> {
    let cmdopts = CmdOptions::parse_args_default_or_exit();

    let config = config::Config::new().unwrap();

    let gpu_backend = config.backend.get_backend();
    let gpu_modes = gpu_backend.list_modes();
    println!("modes: {:?}", gpu_modes);

    // Terminal initialization
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.clear()?;

    let events = Events::new();

    let mut app = App::new(&gpu_modes);

    app.items.state.select(gpu_backend.default());

    let mut pressed = false;
    let mut apply = true;
    let start = Instant::now();
    loop {
        ui::draw(&mut app, &mut terminal);
        //print!("{:?}", app.items.state.selected());
        match events.next()? {
            Event::Input(input) => {
                pressed = true;
                match input {
                    Key::Char('q') => {
                        apply = false;
                        break;
                    }
                    Key::Down => {
                        app.items.next();
                    }
                    Key::Up => {
                        app.items.previous();
                    }
                    Key::Char('\n') => {
                        break;
                    }
                    _ => {}
                }
            }
            Event::Tick => {
                //app.advance();
                if let Some(timeout) = config.timeout {
                    if !pressed && start.elapsed().as_secs() >= timeout {
                        break;
                    }
                }
            }
        }
    }

    terminal.clear()?;
    if apply || config.timeout_apply {
        //println!("{:?}", app.items.state.selected());
        if let Err(err) = gpu_backend.apply_mode(app.items.state.selected()) {
            println!("error switching gpu mode: {:?}", err);
        }
    }

    if let Some(exec) = cmdopts.exec {
        let exec: Vec<&str> = exec.split(" ").collect();
        let err = exec::Command::new(exec[0]).args(&exec[1..]).exec();
        println!("command exec failed: {:?}", err);
    }
    Ok(())
}
