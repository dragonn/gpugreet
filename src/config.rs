use std::error::Error;

use std::fs;

use serde::Deserialize;

use crate::backends::builtin::BuiltinConfig;
use crate::backends::Backend;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type", content = "options")]
pub enum Backends {
    Builtin(BuiltinConfig),
}

impl Backends {
    pub fn get_backend(&self) -> Box<dyn Backend + '_> {
        match self {
            Self::Builtin(config) => Box::new(config.to_backend()),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub backend: Backends,
    pub timeout: Option<u64>,
    #[serde(default)]
    pub timeout_apply: bool,
}

impl Config {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        #[cfg(debug_assertions)]
        return Ok(toml::from_str(&fs::read_to_string("config.toml")?)?);
        #[cfg(not(debug_assertions))]
        return Ok(toml::from_str(&fs::read_to_string("/etc/greetd/gpu/config.toml")?)?);
    }
}
